# project-skills
Application permettant de générer les livrets d'évaluation pour le passage du titre pro DWWM.

Le back est sur du Firestore, normalement pas trop compliqué à remplacer par un back maison.

Possibilité de changer de référentiel en modifiant les compétences dans le fichier referentiel.json et en changeant le fichier public/templates/template-livret.docx
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
